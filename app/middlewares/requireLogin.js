//
// Require Login
//

'use strict';

var passport = require('passport');
var core = require('./../core/index');
function getMiddleware(fail) {
    return function(req, res, next) {
        if (req.user) {
            return next();            
        }


        if (req.headers && req.headers['auth-token']) {
            var token = req.headers['auth-token'];
            return core.account.userFromToken(token, function(err, user) {
                if (err) {
                    return fail(req, res);
                }

                if (!user) {
                    return fail(req, res);                    
                }

                req.user = user;

                var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                user.ipAddress = ip;
                user.save();

                next();
            });
        }

        fail(req, res);
    };
}

module.exports = getMiddleware(function(req, res) {
    try{
        res.sendStatus(401);    
    }
    catch(ex) {

    }    
});
