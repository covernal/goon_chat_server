//
// Require Login
//

'use strict';

var passport = require('passport');
var core = require('./../core/index');
var settings = require('./../config')
function getMiddleware(fail) {
    return function(req, res, next) {
        if (req.user) {
            return next();            
        }

        if (req.headers && req.headers['auth-token']) {
            console.log('requireLoginSuper', settings.admin.token);
            var token = req.headers['auth-token'];
            if (settings.admin && settings.admin.token && settings.admin.token === token) {
                req.user = 'admin';
                return next();
            }
            return core.account.userFromToken(token, function(err, user) {
                if (err) {
                    return fail(req, res);
                }

                if (!user) {
                    return fail(req, res);                    
                }

                var isAdmin = user.roles.some(function(role) {
                    return role === 'admin';
                });
                if (!isAdmin) {
                    return fail(req, res);
                }
                
                req.user = user;
                next();
            });
        }

        fail(req, res);
    };
}

module.exports = getMiddleware(function(req, res) {
    try{
        res.sendStatus(401);    
    }
    catch(ex) {

    }    
});
