//
// User
//

'use strict';

var bcrypt = require('bcryptjs'),
	crypto = require('crypto'),
	md5 = require('md5'),
	hash = require('node_hash'),
	mongoose = require('mongoose'),
	uniqueValidator = require('mongoose-unique-validator'),
	validate = require('mongoose-validate'),
	settings = require('./../config');

const util = require('util')

var ObjectId = mongoose.Schema.Types.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

var UserSchema = new mongoose.Schema({
	app_id: {
		type: ObjectId,
		required: false,
		ref: 'App' 
	},
	username: {
		type: String,
		required: true,
		trim: true,
		lowercase: true,
		match: /^[\w][\w\-\.]*[\w]$/i
	},
	email: {
		type: String,
		required: true,
		trim: true,
		lowercase: true,
		validate: [ validate.email, 'invalid email address' ]
	},
	facebookProvider: {
		type: {
			id: String,
			token: String
		},
		select: false
	},
	password: {
		type: String,
		required: false, // Only required if local
		trim: true//,
		//match: new RegExp(settings.auth.local.passwordRegex)
	},
	token: {
		type: String,
		required: false,
		trim: true
	},
	tokens: {
		type: Mixed,
		default: {}
	},
	firstName: {
		type: String,
		required: true,
		trim: true
	},
	lastName: {
		type: String,
		required: true,
		trim: false
	},
	avatar: {
		type: {
			id: ObjectId,
			url: String
		},
		select: true	
	},
	joined: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: Date.now
	},
	ipAddress: {
		type: String,
		default: '0.0.0.0'
	},
	class: {
		type: String,
		default: "user"
	},
	status: {
		type: String,
		trim: true
	},
	roles: {
		type:[{
			type: String,
			enum: ['user', 'admin']
		}],
		default: ['user'],
		required: 'Please provide at least one role'
	},
	pushCount: {
        type: Number,
        required: true,
        default: 0
	},
	friends: {
		type: Mixed,
		default: {}
	},
	rooms: [{
		type: ObjectId,
		ref: 'Room'
	}],
	blockedRooms: [{
			type: ObjectId,
			ref: 'Room'
	}],
	messages: [{
		type: ObjectId,
		ref: 'Message'
	}]
}, {
	autoIndex: false,
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
});

UserSchema.index({ app_id: 1, username: 1 }, { unique: true });
UserSchema.index({ app_id: 1, email: 1 }, { unique: true });

UserSchema.virtual('jid').get(function() {
	var app_id = "chat.goon";
	return this.username + "@" + app_id;
});

UserSchema.statics.FS_NONE 				= 0b0000;
UserSchema.statics.FS_APRV_FROM_ME 		= 0b0001;
UserSchema.statics.FS_APRV_FROM_FRIEND	= 0b0010;
UserSchema.statics.FS_APRV_FROM_BOTH	= 0b0011;

UserSchema.pre('save', function(next) {
	var user = this;
	if (!user.isModified('password')) {
		return next();
	}
	bcrypt.hash(user.password, 10, function(err, hash) {
		if (err) {
			return next(err);
		}
		user.password = hash;
		next();
	});
});

// UserSchema.pre('save', function(next) {
//	 var user = this;
//	 if (!user.isModified('username')) {
//		 return next();
//	 }
//	 var appDomain = "chat.goon";
//	 user.jid = user.username + "@" + appDomain;
//	 next();
// });

UserSchema.statics.upsertFbUser = function(accessToken, refreshToken, profile, cb) {
  var that = this;
  return this.findOne({
    'facebookProvider.id': profile.id
  }, function(err, user) {
    // no user was found, lets create a new one
    if (!user) {
    console.log("fb user is needed to create".green)
    console.log(profile);
    	var User = mongoose.model('User');
      var newUser = new User({
      	username: profile.id,
        firstName: profile.first_name || profile._raw.first_name || profile.displayName,
        lastName: profile.last_name  || profile._raw.last_name || " ",
        email: profile.emails[0].value,
        facebookProvider: {
          id: profile.id,
          token: accessToken
        }
      });
    console.log(newUser);

      newUser.save(function(error, savedUser) {
        if (error) {
          console.log(error);
        }
        return cb(error, savedUser);
      });
    } else {
      return cb(err, user);
    }
  });
};

UserSchema.statics.searchByUsername = function(pattern, cb) {
	var opts = {};
	opts.username = { $regex: pattern, $options: 'i' };

	this.find(opts, cb);
};

UserSchema.methods.isFriend = function(friend_uid) {
	if (!this.friends[friend_uid]) return false;
	return this.friends[friend_uid].status == this.constructor.FS_APRV_FROM_BOTH;
}

UserSchema.methods.isSentReueste = function(friend_uid) {
	if (!this.friends[friend_uid]) return false;
	return this.friends[friend_uid].status == this.constructor.FS_APRV_FROM_ME;
}

UserSchema.methods.isReceivedRequeste = function(friend_uid) {
	if (!this.friends[friend_uid]) return false;
	return this.friends[friend_uid].status == this.constructor.FS_APRV_FROM_FRIEND;
}

UserSchema.methods.setLastMessage = function(friend_uid, lastMessage, cb) {
	friend_uid = friend_uid.toString();
	if (!this.friends[friend_uid]) return cb(null);

	var thiz = this;
	this.constructor.findByIdentifier(friend_uid, function(err, user) {
		if (err) {
			return cb(err);
		}
		// Does the user exist?
		if (!user) {
			return cb(false);
		}

		if (!user.friends[thiz._id]) return cb(false);

		thiz.friends[friend_uid].lastMessage = lastMessage;
		user.friends[thiz._id].lastMessage = lastMessage;

		thiz.markModified('friends');
		user.markModified('friends');

		thiz.save(function(err, me) {
			if (err) return cb(err);

			user.save(function(err, usr) {
				if (err) return cb(err);

				return cb(null);
			})
		})
	});	
}

UserSchema.methods.addFriend = function(identifier, isForce, cb) {
	var thiz = this;
	this.constructor.findByIdentifier(identifier, function(err, user) {
		if (err) {
			return cb(err);
		}
		// Does the user exist?
		if (!user) {
			return cb(null, null, null);
		}

		if (!thiz.friends[user._id]) {
			thiz.friends[user._id] = {
				user: user._id,
				status: thiz.constructor.FS_APRV_FROM_ME,
			};
		}
		else {
			thiz.friends[user._id].status |= thiz.constructor.FS_APRV_FROM_ME;
		}
		if (!user.friends[thiz._id]) {
			user.friends[thiz._id] = {
				user: thiz._id,
				status: thiz.constructor.FS_APRV_FROM_FRIEND,
			};
		}
		else {
			user.friends[thiz._id].status |= thiz.constructor.FS_APRV_FROM_FRIEND;
		}

		if (isForce) {
			thiz.friends[user._id].status |= thiz.constructor.FS_APRV_FROM_BOTH;
			user.friends[thiz._id].status |= thiz.constructor.FS_APRV_FROM_BOTH;
		}

		thiz.markModified('friends');
		user.markModified('friends');

		thiz.save(function(err, me) {
			if (err) return cb(err);

			user.save(function(err, usr) {
				if (err) return cb(err);

				return cb(null, me, usr, user.friends[thiz._id].status == thiz.constructor.FS_APRV_FROM_BOTH);
			})
		})
	});
}

UserSchema.methods.signActive = function(cb) {
	this.pushCount = 0;
	this.markModified('pushCount');
	this.save(function(err, me) {
		if (!cb) return;
		if (err) return cb(err);
		cb(null);
	});
}

UserSchema.methods.incPushCount = function(cb) {
	this.pushCount++;
	this.markModified('pushCount');
	this.save(function(err, me) {
		if (!cb) return;
		if (err) return cb(err);
		cb(null);
	});
}

UserSchema.methods.removeFriend = function(identifier, cb) {
	var thiz = this;
	this.constructor.findByIdentifier(identifier, function(err, user) {
		if (err) {
			return cb(err);
		}
		// Does the user exist?
		if (!user) {
			return cb(null, null, null);
		}

		if (thiz.friends[user._id]) {
			delete thiz.friends[user._id];
		}
		if (user.friends[thiz._id]) {
			delete user.friends[thiz._id];
		}
		thiz.markModified('friends');
		user.markModified('friends');

		thiz.save(function(err, me) {
			if (err) return cb(err);

			user.save(function(err, usr) {
				if (err) return cb(err);

				return cb(null, me, usr);
			})
		})
	});
}

UserSchema.statics.findByIdentifier = function(identifier, cb) {
	var opts = {};

	if (identifier.match(/^[0-9a-fA-F]{24}$/)) {
		console.log("id or username");
		opts.$or = [{_id: identifier}, {username: identifier}];
	} else if (identifier.indexOf('@') >= 0) {
		console.log("id or username");
		opts.$or = [{jid: identifier}, {email: identifier}];
	}
	else {
		opts.username = identifier;
	}

	this.findOne(opts, cb);
};

UserSchema.methods.generateToken = function(resource, cb) {
	if (!this._id) {
		return cb('User needs to be saved.');
	}

	crypto.randomBytes(24, function(ex, buf) {
		var password = buf.toString('hex');

		bcrypt.hash(password, 10, function(err, hash) {
			if (err) {
				return cb(err);
			}

			var key = "res_" + resource;
			if (!!!this.tokens) this.tokens = {};
			this.tokens[key] = hash;
            this.markModified('tokens');

			var userToken = new Buffer(
				this._id.toString() + ':' + key + ':' + password
			).toString('base64');

			cb(null, userToken);

		}.bind(this));
	}.bind(this));
};

UserSchema.statics.findByToken = function(token, cb) {
	if (!token) {
		return cb(null, null);
	}

	var tokenParts = new Buffer(token, 'base64').toString('ascii').split(':'),
		userId = tokenParts[0],
		resKey = tokenParts[1],
		hash = tokenParts[2];

	if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
		cb(null, null);
	}

	this.findById(userId, function(err, user) {
		if (err) {
			return cb(err);
		}

		if (!user) {
			return cb(null, null);
		}

		bcrypt.compare(hash, user.tokens[resKey], function(err, isMatch) {
			if (err) {
				return cb(err);
			}

			if (isMatch) {
				return cb(null, user);
			}

			cb(null, null);
		});
	});
};

UserSchema.methods.checkTokenValidation = function(token, cb) {
	if (!token) {
		return cb(false);
	}

	var tokenParts = new Buffer(token, 'base64').toString('ascii').split(':'),
		userId = tokenParts[0],
		resKey = tokenParts[1],
		hash = tokenParts[2];

    bcrypt.compare(hash, this.tokens[resKey], function (err, isMatch) {
        if (err) {
            return cb(false);
        }

        if (isMatch) {
            return cb(true);
        }

        cb(false);
    });
};

UserSchema.methods.updatePassword = function (oldpassword, newpassword, cb) {

    var local = settings.auth.local,
        salt = local && local.salt;

    // Legacy password hashes
    if (salt && (hash.sha256(oldpassword, salt) === this.password)) {
        return cb(null, true);
    }
    // Current password hashes
    bcrypt.compare(oldpassword, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }

        if (isMatch) {
            this.password = newpassword;
            this.markModified('password');
            this.save(function (err, me) {
                return cb(err, !!me);
            });
            
        }

        cb(null, false);

    }.bind(this));

};

UserSchema.methods.comparePassword = function(password, cb) {

	var local = settings.auth.local,
		salt = local && local.salt;

	// Legacy password hashes
	if (salt && (hash.sha256(password, salt) === this.password)) {
		return cb(null, true);
	}
	// Current password hashes
	bcrypt.compare(password, this.password, function(err, isMatch) {
		if (err) {
			return cb(err);
		}

		if (isMatch) {
			return cb(null, true);
		}

		cb(null, false);

	});

};

UserSchema.statics.authenticate = function(identifier, password, cb) {
	this.findByIdentifier(identifier, function(err, user) {
		if (err) {
			return cb(err);
		}
		// Does the user exist?
		if (!user) {
			return cb(null, null, 0);
		}
		// // Is this a local user?
		// if (user.provider !== 'local') {
		//	 return cb(null, null, 0);
		// }

		// Is password okay?
		user.comparePassword(password, function(err, isMatch) {
			if (err) {
				return cb(err);
			}
			if (isMatch) {
				return cb(null, user);
			}
			// Bad password
			return cb(null, null, 1);
		});
	});
};

UserSchema.plugin(uniqueValidator, {
	message: 'Expected {PATH} to be unique'
});

// EXPOSE ONLY CERTAIN FIELDS
// It's really important that we keep
// stuff like password private!
UserSchema.method('toJSON', function() {
	return {
		id: this._id,
		jid: this.jid,
		email: this.email,
		firstName: this.firstName,
		lastName: this.lastName,
		username: this.username,
		avatar: this.avatar ? this.avatar.url : null,
		ipAddress: this.ipAddress,
		lastActive: this.updatedAt,
		createdAt: this.createdAt
	};
});

module.exports = mongoose.model('User', UserSchema);