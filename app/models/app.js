//
// App
//

'use strict';

var bcrypt = require('bcryptjs'),
    crypto = require('crypto'),
    md5 = require('md5'),
    hash = require('node_hash'),
    mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    validate = require('mongoose-validate'),
    settings = require('./../config');

var ObjectId = mongoose.Schema.Types.ObjectId;

var AppSchema = new mongoose.Schema({
    name: {
		type: String,
		required: true,
		trim: true
	},
	key: {
		type: String,
		required: false,
	},
	secret: {
		type: String,
		required: false,
	},
	owner: {
		type: String,
		required: true,
		trim: true,
        lowercase: true,
        validate: [ validate.email, 'invalid email address' ]
	}
});
module.exports = mongoose.model('App', AppSchema);