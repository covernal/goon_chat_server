//
// Message
//

'use strict';

var mongoose = require('mongoose'),
    settings = require('./../config');

var ObjectId = mongoose.Schema.Types.ObjectId;

var FCMDeviceSchema = new mongoose.Schema({
    owner: {
        type: ObjectId,
        ref: 'User',
        required: true
    },
    token: {
        type: String,
        required: true,
        unique: true
    }
});

// if (settings.private.expire !== false) {
//     var defaultExpire = 6 * 60; // 6 hours

//     MessageSchema.index({ posted: 1 }, {
//         expireAfterSeconds: (settings.private.expire || defaultExpire) * 60
//     });
// }

FCMDeviceSchema.index({ owner: 1, _id: 1 });

FCMDeviceSchema.statics.deviceOfToken = function (token, cb) {
    var opts = {};
    opts.token = token;
    this.findOne(opts, cb);
}

FCMDeviceSchema.statics.devicesOfUser = function (user_id, cb) {
    var opts = {};
    if (user_id) opts.owner = user_id;
    this.find(opts)
    .exec(function(err, devices) {
        if (err) return cb(err);
        cb(null, devices)
    })
}


// EXPOSE ONLY CERTAIN FIELDS
// This helps ensure that the client gets
// data that can be digested properly
FCMDeviceSchema.method('toJSON', function() {
    return {
        id: this._id,
        owner: this.owner,
        token: this.token
    };
});

module.exports = mongoose.model('FCMDevice', FCMDeviceSchema);
