//
// Users Controller
//

'use strict';

var _ = require('lodash'),
    multer = require('multer'),
    auth = require('./../auth/index'),
    settings = require('./../config');

module.exports = function() {

    var app = this.app,
        core = this.core,
        middlewares = this.middlewares,
        models = this.models,
        User = models.user;

    //
    // Routes
    //
    app.get('/users', function(req, res) {
        var user = {
            id: 1,
            name: "John"
        };
        res.json(user);
    });

    app.post('/signup', function(req, res){
        var fields = req.body || req.data;

        // Sanity check the password
        var passwordConfirm = fields.passwordConfirm || fields.passwordconfirm || fields['password-confirm'];

        if (fields.password !== passwordConfirm) {
            return res.status(400).json({
                status: 'error',
                message: 'Password not confirmed'
            });
        }

        var data = {
                username: fields.username,
                email: fields.email,
                password: fields.password,
                firstName: fields.firstName || fields.firstname || fields['first-name'],
                lastName: fields.lastName || fields.lastname || fields['last-name'],
            };

        core.account.create(data, function(err, user) {
            if (err) {
                var message = 'Sorry, we could not process your request';
                // User already exists
                if (err.code === 11000) {
                    message = 'Username or Email has already been taken';
                }
                // Invalid username
                if (err.errors) {
                    message = _.map(err.errors, function(error) {
                        return error.message;
                    }).join(' ');
                // If all else fails...
                } else {
                    console.error(err);
                }
                // Notify
                return res.status(400).json({
                    status: 'error',
                    message: message
                });
            }
            res.status(201).json({
                status: 'success',
                message: 'You\'ve been registered, ' +
                         'please try logging in now!',
                user: user
            });
        });
    });

    app.post('/login', function(req, res){
        var fields = req.body || req.data;
        var username = fields.username || fields.email;
        var password = fields.password;
        var resource = fields.resource;

        auth.authenticate(username, password, function(err, user, info) {
            if (err) {
                return res.status(400).json({
                    status: 'error',
                    message: 'There were problems logging you in.',
                    errors: err
                });
            }

            if (!user && info && info.locked) {
                return res.status(403).json({
                    status: 'error',
                    user: user,
                    fields: fields,
                    username: username,
                    password: password,
                    info: info,
                    message: info.message || 'Account is locked.'
                });
            }

            if (!user) {
                return res.status(401).json({
                    status: 'error',
                    user: user,
                    fields: fields,
                    username: username,
                    password: password,
                    info: info,
                    message: info && info.message ||
                             'Incorrect login credentials.'
                });
            }

            core.account.generateToken(user._id, resource, function (err, token, user) {
                if (err) {
                    return res.json({
                        status: 'error',
                        message: 'Unable to generate a token.',
                        errors: err
                    });
                }

                var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                user.ipAddress = ip;
                user.save();

                res.json({
                    status: 'success',
                    message: 'Token generated.',
                    token: token,
                    me: user
                });
            });
        });
    });

    app.post('/login/facebook', function(req, res) {
        auth.authFacebook(req, res, function(err, user){
            if (err) {
                console.log("error blabal", err)
                res.status(400).json({
                    status: 'error',
                    message: 'There were problems logging you in.',
                    errors: err
                });
                return;
            }
            else if (!user) {
                console.log("error bad facebook")
                res.status(401).json({
                    status: 'error',
                    message: 'Incorrect login credentials.'
                });
                return;
            }

            var fields = req.body || req.data;
            var resource = fields.resource;
            core.account.generateToken(user._id, resource, function (err, token) {
                if (err) {
                    return res.json({
                        status: 'error',
                        message: 'Unable to generate a token.',
                        errors: err
                    });
                }

                core.account.setTokenToPassword(token, function(err, usr) {
                    if (err) {
                        return res.json({
                            status: 'error',
                            message: 'Unable to generate a token credentials.',
                            errors: err
                        });
                    }

                    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                    user.ipAddress = ip;
                    user.save();

                    res.json({
                        status: 'success',
                        message: 'Token generated.',
                        token: token,
                        me: user
                    });
                })

            });
        })
    })


    app.get('/me', middlewares.requireLogin, function(req, res) {
        res.json(req.user);
    });

    app.post('/me', middlewares.requireLogin, function(req, res) {
        var fields = req.body || req.data;

        // Sanity check the password
        // var passwordConfirm = fields.passwordConfirm || fields.passwordconfirm || fields['password-confirm'];

        // if (fields.password !== passwordConfirm) {
        //     return res.status(400).json({
        //         status: 'error',
        //         message: 'Password not confirmed'
        //     });
        // }

        var data = {
                username: fields.username,
                email: fields.email,
                //password: fields.password,
                firstName: fields.firstName || fields.firstname || fields['first-name'],
                lastName: fields.lastName || fields.lastname || fields['last-name'],
            };
        core.account.update(req.user._id, data, function (err, user) {
            if (err) {
                return res.status(403).json({
                    status: 'error',
                    message: 'Unable to update your profile.',
                    errors: err
                });
            }

            if (!user) {
                return res.sendStatus(404);
            }

            res.json(user);
        });
    });

    app.post('/me/password', middlewares.requireLogin, function (req, res) {
        var fields = req.body || req.data;

        var oldpassword = fields.old;
        var newpassword = fields.new;

        req.user.updatePassword(oldpassword, newpassword, function (err, hasDone) {
            if (err) {
                return res.sendStatus(400).json({
                    status: 'error',
                    message: 'Old password has not matched or there is another problem.'
                    //errors: err
                });
            }
            else {
                return res.json({
                    status: 'success',
                    message: 'Password has updated'
                });
            }
        });
    })

    var fileUpload = multer({
        limits: {
            files: 1,
            fileSize: settings.maxFileSize
        },
        storage: multer.diskStorage({})
    }).any();

    app.post('/me/avatar', middlewares.requireLogin, fileUpload, middlewares.cleanupFiles, function(req, res) {
        if (!req.files) {
            console.log("error on uploading file. no uploaded file.", req.files)
            return res.status(400).json({
                message: 'error on uploading file. no uploaded file.'
            });
        }

        var options = {
                owner: req.user._id,
                //room: req.param('room'),
                file: req.files[0],
                post: (req.param('post') === 'true') && true
            };

        core.files.create(options, function(err, file) {
            if (err) {
                console.log("error on uploading file")
                console.error(err);
                return res.sendStatus(400);
            }
            req.user.avatar = {
                id: file.id,
                url: file.url
            } ;
            req.user.save(function(err, user) {
                if (err) {
                    return res.sendStatus(400).json({
                        message: 'file uploaded, but db failed',
                        file: file
                    });
                }
                core.avatars.add(user);
                res.json(user);
            });
            // res.status(200).json(file);
        });        
    })

    app.post('/me/device', middlewares.requireLogin, function(req, res) {
        var fields = req.body || req.data;
        var token = fields.token;
        core.notifications.addToken(token, req.user, function(err, device){
            if (err) {
                return res.status(403).json({
                    status: 'error',
                    message: 'Failed an adding new device'
                });
            }
            res.json({result: true});
        })
    });

    app.delete('/me/device', middlewares.requireLogin, function(req, res) {
        var fields = req.body || req.data;
        var token = fields.token;
        core.notifications.deleteToken(token, req.user, function(err, msg){
            if (err) {
                return res.status(403).json({
                    status: 'error',
                    message: 'Failed to remove the device'
                });
            }
            res.json({result: true, message:msg});
        })
    });

    app.get('/users/find/:pattern', function(req, res) {
        core.account.searchUser(req.params.pattern, function (err, users) {
            if (err) {
                return res.status(400).json(err);
            }
            res.json({
                count: users.length,
                data: users
            });
        });
    });

    app.post('/friends/invite/:touser', middlewares.requireLogin, function(req, res) {
        core.account.addFriend(req.user, req.params.touser, false, function (err, me, friend) {
            if (err || !me) {
                return res.sendStatus(404);
            }
            res.json({
                    status: 'success',
                    message: 'Invited friend',
                    friend: friend
                });
        });
    });

    app.post('/friends/add/:touser', middlewares.requireLogin, function(req, res) {
        core.account.addFriend(req.user, req.params.touser, true, function (err, me, friend) {
            if (err || !me) {
                return res.sendStatus(404);
            }
            res.json({
                    status: 'success',
                    message: 'Added friend',
                    friend: friend
                });
        });
    });

    app.post('/friends/remove/:friend', middlewares.requireLogin, function(req, res) {
        core.account.removeFriend(req.user, req.params.friend, function (err, me, friend) {
            if (err || !me) {
                return res.sendStatus(404);
            }
            res.json({
                    status: 'success',
                    message: 'Removed friend',
                    friend: friend
                });
        });
    });

    app.get('/friends', middlewares.requireLogin, function(req, res) {
        var me = req.user;
        core.users.listOfFriend(req.user, User.FS_NONE, function(err, users) {
            var friends = [];
            var sent = [];
            var received = [];
            for (var i = 0; i < users.length; i++) {
                if (me.isFriend(users[i].id)) { friends.push(users[i]); }
                if (me.isSentReueste(users[i].id)) { sent.push(users[i]); }
                if (me.isReceivedRequeste(users[i].id)) { received.push(users[i]); }
            }
            res.json({
                    status: 'success',
                    friends: friends,
                    sent: sent,
                    received: received
                });
        });
    });


    app.get('/api/users/all/:skip/:take/:pattern?', middlewares.requireLoginSuper, function(req, res) {
        var options = {
            skip: parseInt(req.params.skip),
            take: parseInt(req.params.take),
        };
        if (req.params.pattern) options.username = new RegExp('^.*' + req.params.pattern + '.*$', "i");
        core.users.list(options, function(err, users) {
            users = users || {};
            var opts = {};
            if (req.params.pattern) opts.username = new RegExp('^.*' + req.params.pattern + '.*$', "i");
            core.users.count(opts, function(err, count){
                res.json({
                    status: 'success',
                    users: users,
                    count: count || 0,
                    err: err
                });
            })
        });
    })
    app.post('/api/users/edit/:id', middlewares.requireLoginSuper, function(req, res) {        
        var fields = req.body || req.data;

        var data = {
                username: fields.username,
                email: fields.email,
                //password: fields.password,
                firstName: fields.firstName || fields.firstname || fields['first-name'],
                lastName: fields.lastName || fields.lastname || fields['last-name'],
            };
        core.account.update(req.params.id, data, function (err, user) {
            if (err) {
                return res.status(403).json({
                    status: 'error',
                    message: 'Unable to update the profile.',
                    errors: err
                });
            }

            if (!user) {
                return res.sendStatus(404);
            }

            res.json(user);
        });
    })
};