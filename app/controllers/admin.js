//
// Users Controller
//

'use strict';

var _ = require('lodash'),
    multer = require('multer'),
    auth = require('./../auth/index'),
    fs = require('fs'),
    yaml = require('js-yaml'),
    yamlWriter = require('write-yaml'),
    settings = require('./../config');

module.exports = function() {

    var app = this.app,
        core = this.core,
        middlewares = this.middlewares,
        models = this.models,
        User = models.user;

    app.get('/api/setting', middlewares.requireLoginSuper, function(req, res) {
        var file;
        var result;
        if (fs.existsSync('settings.yml')) {
            file = fs.readFileSync('settings.yml', 'utf8');
            result = yaml.safeLoad(file) || {};
        } else {
            result = {};
        }
        res.json({
            status: 'success',
            setting: result
        });
    })

    app.post('/api/setting', middlewares.requireLoginSuper, function(req, res) {
        var data = req.body || {};
        yamlWriter('settings.yml', data, function(err) {
            res.json({
                status: 'success'
            });
        });
    })

    app.post('/api/users/edit/:id', middlewares.requireLoginSuper, function(req, res) {        
        var fields = req.body || req.data;

        var data = {
                username: fields.username,
                email: fields.email,
                //password: fields.password,
                firstName: fields.firstName || fields.firstname || fields['first-name'],
                lastName: fields.lastName || fields.lastname || fields['last-name'],
            };
        core.account.update(req.params.id, data, function (err, user) {
            if (err) {
                return res.status(403).json({
                    status: 'error',
                    message: 'Unable to update the profile.',
                    errors: err
                });
            }

            if (!user) {
                return res.sendStatus(404);
            }

            res.json(user);
        });
    })

    var fileUpload = multer({
        limits: {
            files: 1,
            fileSize: settings.maxFileSize
        },
        storage: multer.diskStorage({})
    }).any();

    app.post('/api/users/avatar/:id', middlewares.requireLoginSuper, fileUpload, middlewares.cleanupFiles, function(req, res) {
        if (!req.files) {
            console.log("error on uploading file. no uploaded file.", req.files)
            return res.status(400).json({
                message: 'error on uploading file. no uploaded file.'
            });
        }

        core.users.get(req.params.id, function(err, user) {
            if (err) {
                console.log("Not found user")
                console.error(err);
                return res.sendStatus(400).json({
                    message: 'Not exist user.'
                });
            }

            req.user = user;

            var options = {
                owner: req.user._id,
                //room: req.param('room'),
                file: req.files[0],
                post: (req.param('post') === 'true') && true
            };

            core.files.create(options, function(err, file) {
                if (err) {
                    console.log("error on uploading file")
                    console.error(err);
                    return res.sendStatus(400);
                }
                req.user.avatar = {
                    id: file.id,
                    url: file.url
                } ;
                req.user.save(function(err, user) {
                    if (err) {
                        return res.sendStatus(400).json({
                            message: 'file uploaded, but db failed',
                            file: file
                        });
                    }
                    core.avatars.add(user);
                    res.json(user);
                });
                // res.status(200).json(file);
            });        
        });
    })

    app.post('/api/push/all', function(req, res) {
        var fields = req.body || req.data;
        console.log(fields);
        core.notifications.send2All(fields.title, 1, fields.message, null, function(err, data) {
            res.json({err: err, result: true, data:data});
        });
    });

    app.post('/api/push/device', function(req, res) {
        var fields = req.body || req.data;
        console.log(fields);
        core.notifications.send2Token(fields.token, fields.title, 1, fields.message, null, function(err, data) {
            res.json({err: err, result: true, data:data});
        });
    });
};