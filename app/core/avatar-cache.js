'use strict';

var crypto = require('crypto'),
    mongoose = require('mongoose'),
    http = require('http');

function AvatarCache(options) {
    this.core = options.core;
    this.avatars = {};

    this.get = this.get.bind(this);
    this.add = this.add.bind(this);
}

AvatarCache.prototype.get = function(userId) {
    return this.avatars[userId];
};

AvatarCache.prototype.defaultAvatar = function(user) {
    this.core.emit('avatar-cache:update', user);
    var userId = (user.id || user._id).toString();
    this.avatars[userId] = null;
}    

AvatarCache.prototype.add = function(user) {
    var User = mongoose.model('User');
    var userId = (user.id || user._id).toString();
    console.log(user);
    console.error(user.avatar);
    User.findByIdentifier(userId, function(err, user) {    
        if (user.avatar) {

            this.core.files.read({
                file: user.avatar.id
            }, function(err, data) {
                if (err) {
                    return this.defaultAvatar(user)
                }
                this.avatars[userId] = {
                    base64: data.toString('base64'),
                    sha1: crypto.createHash('sha1').update(data).digest('hex')
                };
                this.core.emit('avatar-cache:update', user);
            }.bind(this))
        }
        else {
            this.defaultAvatar(user)
        }
    }.bind(this))
};



module.exports = AvatarCache;
