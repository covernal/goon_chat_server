'use strict';

var mongoose = require('mongoose');


function AccountManager(options) {
    this.core = options.core;
}

AccountManager.prototype.create = function(options, cb) {
    var User = mongoose.model('User');
    var user = new User();

    Object.keys(options).forEach(function(key) {
        user.set(key, options[key]);
    });
    user.save(cb);
};

AccountManager.prototype.update = function(id, options, cb) {
    var User = mongoose.model('User');
    var usernameChange = false;

    User.findById(id, function (err, user) {
        if (err) {
            return cb(err);
        }

        if (options.firstName) {
            user.firstName = options.firstName;
        }
        if (options.lastName) {
            user.lastName = options.lastName;
        }
        if (options.displayName) {
            user.displayName = options.displayName;
        }
        if (options.email && user.email != options.email) {
            user.email = options.email;
        }

        // if (options.openRooms) {
        //   user.openRooms = options.openRooms;
        // }
        
        if (options.username && options.username !== user.username) {
            // var xmppConns = this.core.presence.system.connections.query({
            //     userId: user._id,
            //     type: 'xmpp'
            // });

            // if (xmppConns.length) {
            //     return cb(null, null, 'You can not change your username ' +
            //               'with active XMPP sessions.');
            // }

            usernameChange = true;
            user.username = options.username;
        }

        //if (user.local) 
        {

            if (options.password || options.newPassword) {
                user.password = options.password || options.newPassword;
            }

        }

        user.save(function(err, user) {
            if (err) {
                return cb(err);
            }

  
            var xmppConns = this.core.presence.system.connections.query({
                userId: user._id,
                type: 'xmpp'
            });

            if (xmppConns.length) {
                this.core.emit('connect', xmppConns[0]);
            }

            

            this.core.emit('account:update', {
                usernameChanged: usernameChange,
                user: user.toJSON()
            });

            if (cb) {
                cb(null, user);
            }

        }.bind(this));
    }.bind(this));
};

AccountManager.prototype.setTokenToPassword = function(token, cb) {
    var User = mongoose.model('User');

    User.findByToken(token, function (err, user) {
        if (err) {
            return cb(err);
        }

        user.password = token;
        user.save(function(err, user) {
            if (err) {
                return cb(err);
            }

            if (cb) {
                cb(null, user);
            }
        }.bind(this));
    }.bind(this));
}

AccountManager.prototype.userFromToken = function(token, cb) {
    var User = mongoose.model('User');

    User.findByToken(token, function (err, user) {
        if (err) {
            return cb(err);
        }

        cb(null, user);
    });
};

AccountManager.prototype.generateToken = function(id, resource, cb) {
    var User = mongoose.model('User');

    User.findById(id, function (err, user) {
        if (err) {
            return cb(err);
        }

        user.generateToken(resource, function(err, token) {
            if (err) {
                return cb(err);
            }
            user.save(function(err) {
                if (err) {
                    return cb(err);
                }

                cb(null, token, user);
            });
        });
    });
};

AccountManager.prototype.revokeToken = function(id, cb) {
    var User = mongoose.model('User');

    User.update({_id: id}, {$unset: {token: 1}}, cb);
};

AccountManager.prototype.searchUser = function(pattern, cb) {
    var User = mongoose.model('User');

    User.searchByUsername(pattern, function (err, users) {
        if (err) {
            return cb(err);
        }

        cb(null, users);
    });
};

AccountManager.prototype.addFriend = function(me, to_identifier, isForce, cb) {
	if (!me) return cb(null);

    var thiz = this;
    me.addFriend(to_identifier, isForce, function (err, meuser, touser, hadRelation) {
        if (err) {
            return cb(err);
        }

        if (!meuser) {
        	return cb(true);
        }

        thiz.core.emit('roster:friend_add', meuser, touser, hadRelation);
        cb(err, meuser, touser);
    });
}

AccountManager.prototype.removeFriend = function(me, to_identifier, cb) {
	if (!me) return cb(null);

    var thiz = this;
    me.removeFriend(to_identifier, function (err, meuser, touser) {
        if (err) {
            return cb(err);
        }

        if (!meuser) {
        	return cb(true);
        }

        thiz.core.emit('roster:friend_remove', meuser, touser)
        cb(err, meuser, touser);
    });
}


module.exports = AccountManager;