'use strict';

var mongoose = require('mongoose'),
		_ = require('lodash'),
		helpers = require('./helpers'),
		FCM = require('fcm-push'),
		settings = require('./../config').fcm;


function NotificationManager(options) {
	this.core = options.core;
	
	console.log('init FireBase Cloud Notification - ' + settings.serverKey);
	this.fcm = settings && settings.serverKey ? new FCM(settings.serverKey) : undefined;

    this.core.on('user-messages:new', function(msg, user, owner, data) {
			var push_msg = msg.text;
			if (msg.text.indexOf('gocommunityupload://0') == 0) {
				push_msg = "sent a photo";
			}
			else if (msg.text.indexOf('gocommunityupload://1') == 0) {
				push_msg = "sent a video";
			}
			else if (msg.text.indexOf('gocommunityupload://2') == 0) {
				push_msg = "sent an audio";
			}
			else if (msg.text.indexOf('gocommunityupload://3') == 0) {
				push_msg = "sent a location";
    		}
    		console.log(user);
    		user.incPushCount();
			console.log(user.pushCount);
			var username = owner.firstName + owner.lastName;
			this.send(user._id, username, user.pushCount, push_msg, null, function(err, data) {
				console.log("sent pushnotification to ", user._id, owner.username, msg.text)
			});
    }.bind(this));

    this.core.on('messages:new', function(msg, room, sender) {
    	var Room = mongoose.model('Room');

    	Room.findById(room._id, function(err, room) {
    		if (err) return;
    		if (!room) return;
			var participants = room.participants || [];
			var username = sender.firstName + sender.lastName;
			var push_msg = username + ": " + msg.text;
			if (msg.text.indexOf('gocommunityupload://0') == 0) {
				push_msg = username + " sent a photo";
			}
			else if (msg.text.indexOf('gocommunityupload://1') == 0) {
				push_msg = username + " sent a video";
			}
			else if (msg.text.indexOf('gocommunityupload://2') == 0) {
				push_msg = username + " sent an audio";
			}
			else if (msg.text.indexOf('gocommunityupload://3') == 0) {
				push_msg = username + " sent a location";
    		}    		
    		participants.forEach(function(user_id, index) {
				if (user_id.toString() == sender._id.toString()) return;
				console.log(user_id);
				var User = mongoose.model('User');
			    User.findByIdentifier(user_id + '', function(err, user) { 
			    	user.incPushCount();
					this.send(user_id, room.name, 1, push_msg, null, function(err, data) {
						console.log("sent pushnotification to ", user_id, sender.username, msg.text)
					});
			    }.bind(this)); 
    		}.bind(this));
    	}.bind(this));
    }.bind(this));
}

function send(fcm, device, title, message, badgeCount, data, cb) {
	if (!!!fcm) {
		return; // cb(true);
	}
	var token = device.token ||  device;
	var payload = {
	    to: token,
	    //collapse_key: 'your_collapse_key', 
	    //data: data,
	    priority : "high",  
	    notification: {
	        title: title,
	        body: message,
	        "sound" : "bingbong.aiff",
	    	badge: badgeCount,		        
	    },
	    time_to_live : 3
	};
	if (data) payload.data = data;

	//callback style
	console.log("tried sending push", token)
	fcm.send(payload, function(err, response){
	    if (err) {
	    	if (err == "InvalidRegistration" || err == "NotRegistered") {
	    		if (device.remove) device.remove(function(err) { })
				console.log(token, "fcm failed with response: ", err);
	    	}
	    } else {
	        console.log(token, "Successfully sent with response: ", response);
	    }
	    //cb(err, device.token)
	});
}

NotificationManager.prototype.addToken = function(token, user, cb) {
	var FCMDevice = mongoose.model('FCMDevice');
	FCMDevice.deviceOfToken(token, function(err, device) {
		if (err) return cb(err);
		if (!device) {
			var data = {
				owner: user._id,
				token: token
			}
			device = new FCMDevice(data);
			device.save(function(err) {
				if (err) return cb(err);
				cb(null, device);
			})
		} else {
			if (device.owner == user._id) return cb(null, device);
			device.owner = user._id;
			device.save(function(err) {
				if (err) return cb(err);
				cb(null, device);
			})
		}
	})
}

NotificationManager.prototype.deleteToken = function(token, user, cb) {
	var FCMDevice = mongoose.model('FCMDevice');
	FCMDevice.deviceOfToken(token, function(err, device) {
		if (err) return cb(err, '');
		if (!device) {
			cb(null, "Not found such device");
		} else {
			console.log(device, user);
			if (device.owner.toString() != user._id.toString()) return cb(null, "You have not linked to this device");
			device.remove(function(err) {
				if (err) return cb(err, '');
				cb(null, device);
			})
		}
	})
}

NotificationManager.prototype.send = function(user_id, title, badgeCount, message, data, cb) {
	if (!!!this.fcm) {
		return cb('FCM not initialized');
	}
	var FCMDevice = mongoose.model('FCMDevice');
	FCMDevice.devicesOfUser(user_id, function(err, devices) {
		if (err) return cb(err);
	    var tokens = [];
	    for (var i = devices.length - 1; i >= 0; i--) {
	        tokens.push(devices[i].token)
	        send(this.fcm, devices[i], title, message, badgeCount, data);
	    }    
	    //send(this.fcm, tokens, title, message, data);
		return cb(null, tokens);
	}.bind(this));
}

NotificationManager.prototype.send2Token = function(fcmToken, title, badgeCount, message, data, cb) {
    var tokens = [fcmToken];
    for (var i = tokens.length - 1; i >= 0; i--) {
        send(this.fcm, tokens[i], title, message, badgeCount, data);
    }    
    //send(this.fcm, tokens, title, message, data);
	return cb(null, tokens);
}

NotificationManager.prototype.send2All = function(title, badgeCount, message, data, cb) {
    this.send(null, title, badgeCount, message, data, cb);
}

module.exports = NotificationManager;