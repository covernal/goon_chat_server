'use strict';

var mongoose = require('mongoose'),
    helpers = require('./helpers');

function UserManager(options) {
    this.core = options.core;
}

UserManager.prototype.list = function(options, cb) {
    options = options || {};

    options = helpers.sanitizeQuery(options, {
        defaults: {
            take: 500
        },
        maxTake: 5000
    });

    var User = mongoose.model('User');

    var opts = {}
    if (options.username) {
        opts.username = options.username;
    }

    console.log(opts);

    var find = User.find(opts);

    if (options.skip) {
        find.skip(options.skip);
    }

    if (options.take) {
        find.limit(options.take);
    }

    find.exec(cb);
};

UserManager.prototype.count = function(options, cb) {
    options = options || {};

    var User = mongoose.model('User');
    User.count(options, cb);
};

UserManager.prototype.listOfFriend = function(user, filter, cb) {
    var User = mongoose.model('User');
    User.findById(user.id, function(err, _user) {
        var friends = _user.friends;
        var keys = [];
        for(var key in friends) {
            if(!friends.hasOwnProperty(key)) continue;
            var relation = friends[key];
            if (relation.status == filter || filter == User.FS_NONE) {
                keys.push(mongoose.Types.ObjectId(key));
            }
        }

        User.find({
            '_id': { $in: keys}
        }, function(err, docs){
            if (err) cb(err);
            var result = docs.map(function(friend) {
                var f = friend.toJSON();
                f.lastMessage = friends[friend.id].lastMessage ? friends[friend.id].lastMessage : "";
                f.unreadCount = 0;

                var relation = friends[friend.id];
                f.subscription = relation.status == User.FS_APRV_FROM_BOTH ? 'both' : (relation.status == User.FS_APRV_FROM_FRIEND ? 'from' : 'to')
                
                return f;
            })
            console.log(result);
            cb(null, result);
        });
    })
}

UserManager.prototype.get = function(identifier, cb) {
    var User = mongoose.model('User');
    User.findById(identifier, cb);
};

UserManager.prototype.username = function(username, cb) {
    var User = mongoose.model('User');
    User.findOne({
        username: username
    }, cb);
};

UserManager.prototype.signActive = function(username, cb) {
    var User = mongoose.model('User');
    User.signActive(cb);
};

module.exports = UserManager;
