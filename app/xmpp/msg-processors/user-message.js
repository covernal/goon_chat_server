'use strict';

var _ = require('lodash'),
    MessageProcessor = require('./../msg-processor'),
    Stanza = require('node-xmpp-server').Stanza,
    settings = require('./../../config');

module.exports = MessageProcessor.extend({

    if: function() {
        return this.request.name === 'message' &&
               this.request.type === 'chat' &&
               !this.toARoom &&
               this.request.attrs.to;
    },

    then: function(cb) {
        if (!settings.private.enable) {
            return cb();
        }

        var username = this.request.attrs.to.split('@')[0];

        var body = _.find(this.request.children, function (child) {
            return child.name === 'body';
        });

        // var composing = _.find(this.request.children, function (child) {
        //     return child.name === 'composing';
        // });

        // if (!body) {// && !composing) {
        //     return cb();
        // }


        if (body) {
            this.core.users.username(username, function(err, user) {
                if (err) {
                    return cb(err);
                }

                if (!user) {
                    return cb();
                }

                this.core.usermessages.create({
                    owner: this.connection.user.id,
                    user: user._id,
                    text: body.text(),
                    data: {
                        id: this.request.attrs.id
                    }
                }, function(err) {
                    cb(err);
                });

            }.bind(this));
            return;
        }

        var connections = this.core.presence.system.connections.query({
            user: username,
            type: 'xmpp'
        });
        connections.forEach(function(connection) {
            // var stanza = new Stanza('message', {
            //     id: this.request.attrs.id,
            //     from: this.connection.jid(),
            //     to: connection.jid(),
            //     type: "chat"
            // });
            // var v = stanza.c('composing', {
            //     xmlns: 'http://jabber.org/protocol/chatstates'
            // });
            var stanza = this.request;
            stanza.attr('from', this.connection.jid());
            connection.client.send(stanza);
            console.log(stanza.root().toString().green);
        }, this);
    }
});
