'use strict';

var _ = require('lodash'),
    MessageProcessor = require('./../msg-processor'),
    Stanza = require('node-xmpp-server').Stanza,
    settings = require('./../../config');

module.exports = MessageProcessor.extend({

    if: function() {
        return this.request.name === 'message' &&
               this.request.type === 'system';
    },

    then: function(cb) {
        var connections = this.core.presence.system.connections.query({
            type: 'xmpp'
        });
        var stanza = this.request;
        console.log('delivery system message as bulk to all users');
        console.log(stanza.root().toString().green);
        connections.forEach(function(connection) {
            connection.client.send(stanza);
        }, this);
    }
});
