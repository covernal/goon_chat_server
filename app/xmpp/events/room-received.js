'use strict';

var Message = require('node-xmpp-server').Message,
    EventListener = require('./../event-listener');

var mentionPattern = /\B@(\w+)(?!@)\b/g;

module.exports = EventListener.extend({

    on: 'room:received',

    then: function(room, user, data) {
        var received_id = data;
        var connections = this.getConnectionsForRoom(room._id);

        console.log("room:received ", received_id);

        connections.forEach(function(connection) {
            var stanza = new Message({
                type: 'groupchat',
                to: connection.getRoomJid(room.slug),
                from: connection.getRoomJid(room.slug, user.username)
            });

            stanza.c('received', {
                xmlns: 'urn:xmpp:receipts',
                id: received_id
            });

            this.send(connection, stanza);

        }, this);
    }

});
