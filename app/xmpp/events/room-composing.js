'use strict';

var Message = require('node-xmpp-server').Message,
    EventListener = require('./../event-listener');

var mentionPattern = /\B@(\w+)(?!@)\b/g;

module.exports = EventListener.extend({

    on: 'room:composing',

    then: function(room, user, data) {
        var connections = this.getConnectionsForRoom(room._id);

        console.log("room:composing", connections.length);

        connections.forEach(function(connection) {
            var stanza = new Message({
                type: 'groupchat',
                to: connection.getRoomJid(room.slug),
                from: connection.getRoomJid(room.slug, user.username)
            });

            stanza.c('composing', {
                xmlns: 'http://jabber.org/protocol/chatstates'
            });

            this.send(connection, stanza);

        }, this);
    }

});
