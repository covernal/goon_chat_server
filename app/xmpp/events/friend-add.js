'use strict';

var Presence = require('node-xmpp-server').Presence,
    Stanza = require('node-xmpp-server').Stanza,
    EventListener = require('./../event-listener');

module.exports = EventListener.extend({

    on: 'roster:friend_add',

    then: function(owner, friend, isBoth) {
        var connections = this.core.presence.system.connections.query({
            userId: owner._id.toString(),
            type: 'xmpp'
        });
        connections.forEach(function(connection) {
            var stanza = new Stanza('iq', {
                id: owner._id + friend._id ,
                to: connection.getUserJid(owner.username),
                type: "set"
            });
            var v = stanza.c('query', {
                xmlns: 'jabber:iq:roster',
                'ver': 'ver13'
            });
            v.c('item', {
                jid: connection.getUserJid(friend.username),
                name: friend.displayName,
                subscription: isBoth ? 'both' : 'to'
            }).c('group').t('GoonChat');

            this.send(connection, stanza);
        }, this);


        connections = this.core.presence.system.connections.query({
            userId: friend._id.toString(),
            type: 'xmpp'
        });
        connections.forEach(function(connection) {
            var stanza = new Stanza('iq', {
                id: friend._id + owner._id,
                to: connection.getUserJid(friend.username),
                type: "set"
            });
            var v = stanza.c('query', {
                xmlns: 'jabber:iq:roster',
                'ver': 'ver13'
            });
            v.c('item', {
                jid: connection.getUserJid(owner.username),
                name: owner.displayName,
                subscription: isBoth ? 'both' : 'from'
            }).c('group').t('GoonChat');
            this.send(connection, stanza);
        }, this);
    }

});
