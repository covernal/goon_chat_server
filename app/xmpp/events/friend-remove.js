'use strict';

var Presence = require('node-xmpp-server').Presence,
    Stanza = require('node-xmpp-server').Stanza,
    EventListener = require('./../event-listener');

module.exports = EventListener.extend({

    on: 'roster:friend_remove',

    then: function(owner, friend) {

console.log(owner, friend);

        var connections = this.core.presence.system.connections.query({
            userId: owner._id.toString(),
            type: 'xmpp'
        });

        connections.forEach(function(connection) {
            var presence = new Presence({
                from: connection.getUserJid(friend.username),
                to: connection.getUserJid(owner.username),
                type: 'unsubscribe'
            });
            this.send(connection, presence);
        }, this);

        connections = this.core.presence.system.connections.query({
            userId: friend._id.toString(),
            type: 'xmpp'
        });

        connections.forEach(function(connection) {
            var presence = new Presence({
                from: connection.getUserJid(owner.username),
                to: connection.getUserJid(friend.username),
                type: 'unsubscribe'
            });
            this.send(connection, presence);
        }, this);

    }

});
