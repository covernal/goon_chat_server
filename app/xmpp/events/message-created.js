'use strict';

var Message = require('node-xmpp-server').Message,
    EventListener = require('./../event-listener');

var mentionPattern = /\B@(\w+)(?!@)\b/g;

module.exports = EventListener.extend({

    on: 'messages:new',

    then: function(msg, room, user, data) {
        var connections = this.getConnectionsForRoom(room._id);

        console.log("room message", connections.length);
        console.log("room message", msg);

        connections.forEach(function(connection) {
            var text = msg.text;

            var mentions = msg.text.match(mentionPattern);

            if (mentions && mentions.indexOf('@' + connection.user.username) > -1) {
                text = connection.nickname(room.slug) + ': ' + text;
            }

            var id = msg._id;
            // if (connection.user.username === user.username) {
                id = data && data.reqest_id || id;
            // }

            var stanza = new Message({
                id: id,
                type: 'groupchat',
                to: connection.getRoomJid(room.slug),
                from: connection.getRoomJid(room.slug, user.username)
            });

            stanza.c('active', {
                xmlns: 'http://jabber.org/protocol/chatstates'
            });

            if (data.received_id) {
                stanza.c('received', {
                    xmlns: 'urn:xmpp:receipts',
                    id: data.received_id
                });
            } else {
                stanza.c('request', {
                    xmlns: 'urn:xmpp:receipts'
                });
            }

            stanza.c('body').t(text);

            stanza.c('identifier', {
                id: msg._id,
                stanza_id: id
            });

            this.send(connection, stanza);

        }, this);
    }

});
